<?php

/**
* This controller created by Shorif, 10/07/2019
*/

use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Save_order extends REST_Controller
{
	function __construct($config = 'rest')
	{
		parent::__construct($config);
		$this->load->model('save_order_model');
		$this->load->helper('url');
		$this->load->model('login_model');
	}
	protected $rest_format   = 'application/json';

	function index_post(){

		if( $this->request->body){
			$requestData = $this->request->body;
			$requestData = $requestData[0];
		}else{
			$requestData = $this->input->post();
		}

		// var_dump($requestData['delevery_date']);delevery_date
		// die();

		$username = $this->input->get_request_header('username');
		$password = $this->input->get_request_header('password');
		
		$isValidUser = $this->login_model->getUser($username, $password);
		$response = array();

			if(!empty($isValidUser)){

				$data1 = array('txid' => $requestData['txid'],
					'product_id' => $requestData['product_id'],
					'quantityes' => $requestData['quantityes'],
					'client_id' => $requestData['client_id'],
					'taker_id' => $requestData['taker_id'],
					'delevary_date' => $requestData['delevary_date'],
					'plant' => $requestData['plant'],
					'taking_date' => $requestData['taking_date'],
					'order_type' => $requestData['order_type']
				);

				$data2 = array('p_name' => $requestData['p_name'],
					'p_type' => $requestData['p_type']
				);

				$res1 = $this->save_order_model->insertOrderTable($data1);
				$res2 = $this->save_order_model->insertProductDetails($data2);

				if(!empty($res1) && !empty($res2)){
					$response['message'] = "Successfully saved data";
				} else {
					$response['message'] = "Failed to save data";
				}
			} else {
				$response['message'] = "Username or password not valid";
			}

			echo json_encode($response);

	}

}
