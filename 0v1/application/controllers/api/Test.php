<?php 
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';


class Test extends REST_Controller
{
	var $data;
	private $email = "";
	
	function __construct($config = 'rest') 
	{
		parent::__construct($config);
        
	}
		
	protected $rest_format   = 'application/json';

    public function index_get(){
		$ret = 'GET request.';
		$this->response($ret, 200);
	}
	
	public function index_post(){
		$ret = "Post request called.";
		$this->response($ret, 200);
    }
	
}

?>