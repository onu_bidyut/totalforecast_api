<?php


use Restserver\Libraries\REST_Controller;

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';


class EmployeeWiseOrder extends REST_Controller{
	function __construct($config = 'rest')
	{
		parent::__construct($config);
		$this->load->model('Employee_wise_order_model');
		$this->load->helper('url');
		$this->load->model('Login_model');
	}

	function index_get(){
		$username = $this->input->get_request_header('username');
		$password = $this->input->get_request_header('password');
		$start_date = $this->input->get_request_header('start_date');
		$end_date = $this->input->get_request_header('end_date');
		$employee_id = $this->input->get_request_header('designation');
		$order_type = $this->input->get_request_header('order_type');

		$isValidUser = $this->Login_model->getUser($username, $password);

		$response = array();

		if(!empty($isValidUser)){
			$data_employee_id = $employee_id;
			$data_start_date = $start_date;
			$data_end_date = $end_date;
			$data_order_type = $order_type;
			$request = $this->Employee_wise_order_model->getUser($data_employee_id, $data_start_date, $data_end_date, $order_type);
		}
		else {
			$response['message'] = "User does not exist";
		}

		// echo json_encode($request);

		echo json_encode($request);
		
	}
}