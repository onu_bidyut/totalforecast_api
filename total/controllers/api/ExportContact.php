<?php 
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';


class ExportContact extends REST_Controller
{
	var $data;
	private $email = "";
	
	function __construct($config='rest') 
	{
		parent::__construct($config);
		$this->load->model('contact_model');
		$this->load->helper('url');
        
	}
		
	protected $rest_format   = 'application/json';

    public function index_get(){

		$userId = $this->input->get_request_header('Userid');
		$Return = $this->contact_model->ExportContact($userId);

		foreach ($Return as $item) {
			$contact['profile'] = $item;
			$contactList['contact_list'][] = $contact;
		}

		$this->response($Return, 200);
	}
	
}

?>