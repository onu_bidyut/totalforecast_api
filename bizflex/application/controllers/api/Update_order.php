<?php

/**
* This controller created by Shorif, 11/07/2019
*/

use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Update_order extends REST_Controller
{
	function __construct($config = 'rest')
	{
		parent::__construct($config);
		$this->load->model('update_order_model');
		$this->load->helper('url');
		$this->load->model('login_model');
	}
	protected $rest_format   = 'application/json';

	function index_post(){

		if( $this->request->body){
			$requestData = $this->request->body;
			$requestData = $requestData[0];
		}else{
			$requestData = $this->input->post();
		}

		$username = $this->input->get_request_header('username');
		$password = $this->input->get_request_header('password');
		
		$isValidUser = $this->login_model->getUser($username, $password);
		$response = array();
		if(!empty($isValidUser)){
			$data1 = array('txid' => $requestData['txid'],
				'product_id' => $requestData['product_id'],
				'quantityes' => $requestData['quantityes'],
				'client_id' => $requestData['client_id'],
				'taker_id' => $requestData['taker_id'],
				'delevary_date' => $requestData['delevary_date'],
				'plant' => $requestData['plant'],
				'taking_date' => $requestData['taking_date'],
				'order_type' => $requestData['order_type']
			);

			$data2 = array('p_name' => $requestData['p_name'],
				'p_type' => $requestData['p_type']
			);

			$res1 = $this->update_order_model->updateOrderTable($data1, $username, $password);
			$res2 = $this->update_order_model->updateProductDetails($data2, $username, $password);

			if(!empty($res1) && !empty($res2)){
				$response['message'] = "Successfully updated data";
			} else {
				$response['message'] = "Failed to updated data";
			}
		} else {
			$response['message'] = "Username or password not valid";
		}

		echo json_encode($response);
	}
}
