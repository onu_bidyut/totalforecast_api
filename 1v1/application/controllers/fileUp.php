<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
// require APPPATH.'/libraries/REST_Controller.php';

class ImgFileSave extends CI_Controller
{
	private $email = "";
	
	function __construct() 
	{
		parent::__construct();				
		$this->isSuccessfulLogin = false;		
		$this->load->helper('url');
		$this->load->model('login_model');
		$this->load->model('payment_model');
	}
		
	 protected $rest_format   = 'application/json';
	 
	 //protected $methods              = array('HTTP/1.1');
	
	public function index()
	{
		 if(empty($_POST['username']) || !isset($_POST['password']))
		 {
			$resonseStatus = 4400 ;
			$resonseReason = 'Invalid format' ;
			// die('bhejal');
		 }
		 else
		 {
			 try
			 {

				$imgInfo = array();
				$username = $_POST['username'];
				$password = $_POST['password'];
				$trxid = $_POST['trxid'];
				$userID = $_POST['user_id'];
				$extension=$_POST['extension'];
				$file_detail=$_POST['file_detail'];
				$request_time=$_POST['request_time'];
				$fileType = $_POST['image_type'];
			
				$server_url= dirname(__FILE__); 
				// die($server_url);
				//$folder_path = $server_url.'//..//..//..//recorder.onuserver.com';
				$folder_path = '/srv/users/serverpilot/apps/clients/public/payflex/asset/images';
				//check if is_dir returns valid path for our folder path so far. 
				
				$file_path = $folder_path .'/'. $username;	

				//create user name filepath and check if already exists, otherwise create

				//Checking if folder already exist----------------------------------------
				if (!file_exists($file_path )) 
				{
					mkdir($file_path , 0777, true);
					
				}

				//create device id file path , check already exists and then create if necessary. 
			
	
				$devide = explode('.',$_FILES['uploaded_file']['name']);
				$uploadTo = $file_path .'/'. $trxid . '.'.$devide[1];
					
				$resonseStatus = 4400 ;
				$resonseReason = 'Failed to upload' ;
				
				
				if(move_uploaded_file($_FILES['uploaded_file']['tmp_name'],$uploadTo))	
				{	
					$resonseStatus = 4000 ;
					$resonseReason = 'File uploaded';
					$request_file_size = $_FILES['uploaded_file']['size'];
					$file_size = filesize($uploadTo);
					$fileExists = file_exists($uploadTo);
					$imgInfo['image_url']=base_url()."asset/images/". $username.'/'.$trxid.".jpg";
					$imgInfo['trxid']=$trxid;
					$imgInfo['user_id']=$userID;
					$imgInfo['image_name']=$trxid.".jpg";
					$imgInfo['image_size']=$file_size;
					$imgInfo['relative_path']=$file_path.'/'.$trxid.".jpg";
					$imgInfo['image_discription']=$file_detail;
					$imgInfo['request_time']=$request_time;
					$imgInfo['upload_time']=$request_time;
					$imgInfo['image_type']=$fileType;
					$this->payment_model->saveClientImagInfo($imgInfo);
				}
				
				$responseContent['status'] = $resonseStatus;
				$responseContent['info'] = $resonseReason;
				$responseContent['file_name'] = $trxid;
				// $responseContent['file_path'] = $uploadTo;
                $responseContent['request_file_size'] = $request_file_size;
				$responseContent['file_size'] = $file_size;
				$responseContent['file_exists'] = $fileExists;
				 
				//array_push($sms_array,$responseContent);
				$myJSonDatum = json_encode($responseContent);
			 }
			 catch(Exception $ex)
			 {
				 // add error status and message
			 	//for dev print stack
			 }	
		 }
		 $this->response($myJSonDatum, 200);
		 //print_r($myJSonDatum); // OK (200) being the HTTP response code
		// die;
	}
	
	
}