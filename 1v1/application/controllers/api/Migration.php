<?php 
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';


class Migration extends REST_Controller
{
	var $data;
	private $email = "";
	
	function __construct($config='rest') 
	{
		parent::__construct($config);
		$this->load->model('contact_model');        
		$this->load->model('migration_model');        
	}
		
	protected $rest_format   = 'application/json';

	
	public function index_post(){

		// $PostData = $this->request->body;
		// $PostData = $this->input->post();

		if($this->request->body){
			$PostData = $this->request->body;
		}else{
			$PostData = $this->input->post();
		}
		
		$Contacts = $PostData['Contacts'];

		$i = 0;
		foreach($Contacts as $contact){
			$this->SyncContact($contact);
			$i++;
		}
		$responseData['status'] = 4000;
		$responseData['syncSuccessfully'] = $i;
		$responseData['syncFailed'] = sizeof($Contacts) - $i;

		$this->response($responseData, 200);	
	}

	function syncContact($contact){
		$basicInfo = $this->migration_model->basicInfoParser($contact);
		$contactId = $this->migration_model->InsertContact($basicInfo);

		$physicalAddress = $this->migration_model->physicalAddressParser($contact, $contactId);
		$this->contact_model->InsertContactInfo('tbl_physical_contact', $physicalAddress);

		$virtualAddress = $this->migration_model->virtualAddressParser($contact, $contactId);
		$this->contact_model->InsertVirtualContact('tbl_virtual_contact', $virtualAddress);

		$relation = $this->migration_model->relationParser($contact, $contactId);
		$this->contact_model->InsertContactInfo('tbl_relation_with_contact', $relation);

		$profession = $this->migration_model->professionParser($contact, $contactId);
		$this->contact_model->InsertContactInfo('tbl_profession', $profession);
	}	
}

?>