<?php
/**
 * Created by PhpStorm.
 * User: 1992b
 * Date: 7/5/2019
 * Time: 9:45 PM
 */
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
class All_client_orders extends REST_Controller{
	var $data;
	function __construct($config = 'rest')
	{
		parent::__construct($config);
		$this->load->model('client_model');
		$this->load->helper('url');
		$this->load->model('login_model');
	}

	protected $rest_format = 'application/json';
	
	function _perform_library_auth( $email = '', $password = NULL)
	{			
		$CI = get_instance();
		$CI->load->library('encrypt');
		$CI->load->model('login_model');

		$isValidUser = $this->login_model->getUser($email, $password);
		
		if(empty($isValidUser)){
			$response=array();
			$response['message']="Login error!";
			$this->response($response, 401); 
			return false;
		}
		else{
			return true;
		}
	}

	public function index_post()
	{
		if( $this->request->body){
			$requestData = $this->request->body;
		}else{
			$requestData = $this->input->post();
		}
		$requestData = json_decode(file_get_contents('php://input'),true);
		$codedEmployeeId = $requestData['codedEmployeeId'];
		$start_date = $requestData['startDate'];
		$end_date = $requestData['endDate'];
		$response=array();
		$response=$this->client_model->getClientOrderDetails($codedEmployeeId,$start_date,$end_date);
		$this->response(json_encode($response),302);
	}
}
?>
